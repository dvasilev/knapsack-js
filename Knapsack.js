//var knapsackCapacity = 165;
//var knapsackWeights = [23, 31, 29, 44, 53, 38, 63, 85, 89, 82];
//var knapsackProfits = [92, 57, 49, 68, 60, 43, 67, 84, 87, 72];
//var optimalSolution = [1, 1, 1, 1, 0, 1, 0, 0, 0, 0];
//var potentialSolution = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var numberOfThings;
var knapsackWeights = [];
var knapsackProfits = [];
var potentialSolution = [];
var result = [];
var numberOfThingsFromTextBox = parseInt($("select").val());

for (var i = 0; i < numberOfThingsFromTextBox; i++)
    potentialSolution[i] = 0;


function Knapsack() {

    var currentMaxProfit = -1;

    function Process(potentialSolution, numberOfThings) { //проверка стоимости найденного потенциального решения и сравнение стоимости этого решения с текущим максимальным

        var profit = 0;
        var weight = 0;

        for (var i = 0; i < potentialSolution.length; i++) {
            profit += potentialSolution[i] * knapsackProfits[i];
            weight += potentialSolution[i] * knapsackWeights[i];
        }

        if ((weight <= knapsackCapacity) && (profit >= currentMaxProfit)) {
            currentMaxProfit = profit;
            for (var j = 0; j < potentialSolution.length; j++) {
                result[j] = potentialSolution[j];
            }
        }



    };

    this.ProcessBinaryString = function(potentialSolution, numberOfThings) { //генерация бинарных строк в количестве 2^numberOfThings

        if (numberOfThings == 0) {
            Process(potentialSolution, numberOfThings);
        } else {
            potentialSolution[numberOfThings - 1] = 0;
            this.ProcessBinaryString(potentialSolution, numberOfThings - 1);
            potentialSolution[numberOfThings - 1] = 1;
            this.ProcessBinaryString(potentialSolution, numberOfThings - 1);
        }

    };

}

function ChangeIndex() {



    this.AddTextBox = function(numberOfThingsFromTextBox) {

        var divCapacity = $('<div></div>').attr('id', 'divCapacity');
        $('body').append(divCapacity);
        var inputCapacity = $('<input></input>').attr({
            'type': 'text',
            'id': 'inputCapacity'
        });
        $('#divCapacity').append(inputCapacity);

        for (var i = 1; i < numberOfThingsFromTextBox + 1; i++) {
            var div = $('<div></div>').attr('id', 'div' + i);
            $('body').append(div);
            var inputWeight = $('<input></input>').attr({
                'type': 'text',
                'id': 'inputWeight' + i,
            });
            var inputProfit = $('<input></input>').attr({
                'type': 'text',
                'id': 'inputProfit' + i,
            });
            $('#div' + i).append(inputWeight);
            $('#div' + i).append(inputProfit);
        }

        var buttonPush = $('<button>Нжами для подсчета</button>').attr({
            'onclick': 'ExecutePull()',
            'text': '123'
        });
        $('body').append(buttonPush);

    }

    this.PullWeightProfitArray = function() {

        knapsackCapacity = $('#inputCapacity').val();
        var numberOfThingsFromTextBox = parseInt($("select").val());

        for (var i = 1; i < numberOfThingsFromTextBox + 1; i++) {
            knapsackWeights[i - 1] = $('#inputWeight' + i).val();
            knapsackProfits[i - 1] = $('#inputProfit' + i).val();
        }

        numberOfThings = knapsackWeights.length;

        for (var i = 0; i < numberOfThings; i++) {
            potentialSolution[i] = 0;
        }

    }

}

function Execute() {
    var numberOfThingsFromTextBox = parseInt($("select").val());

    //var knapsack = new Knapsack();
    // knapsack.ProcessBinaryString(potentialSolution, numberOfThings);
    var changeIndex = new ChangeIndex();
    changeIndex.AddTextBox(numberOfThingsFromTextBox);
}

function ExecutePull() {
    var numberOfThingsFromTextBox = parseInt($("select").val());
    var changeIndex = new ChangeIndex();
    changeIndex.PullWeightProfitArray();

    var knapsack = new Knapsack();
    knapsack.ProcessBinaryString(potentialSolution, numberOfThings);

    document.write(result);
}
